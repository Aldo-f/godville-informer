# GodVille Informer

Simple [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey) script for [GodVille](https://godvillegame.com/) to notify

## Install

You will need [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey) for Firefox or _who-knows-what_ for other browsers first.

Then click [here](./godville-informer.user.js) to install script.

## License

Copyright © 2019 Aldo

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.
