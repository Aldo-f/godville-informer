// ==UserScript==
// @name        Godville informer
// @namespace   https://github.com/aldo-f/godville-informer
// @description Informer
// @include     https://godvillegame.com/superhero
// @version     0.0.1
// @grant       none
// ==/UserScript==

// settings
var DEBUG = true;
var minHealth = 10;
var pauseTime = 15000;
var maxPower = 95;

function checkHealth() {
  if (DEBUG)
    console.log("I'm checking the hero's health and if godpower is max");

  // health
  var healthDiv = document.getElementById("hk_health");
  var healthVal = healthDiv.getElementsByClassName("l_val")[0].innerHTML;
  var health = healthVal.split(" / ")[0];
  var possibleHealth = healthVal.split(" / ")[1];
  var health_percents = (health * 100) / possibleHealth;
  if (health_percents < minHealth) {
    inform();
  }

  // godpower
  var powerVal = parseInt(
    document.getElementsByClassName("gp_val l_val")[0].innerHTML.split("%")[0]
  );
  if (powerVal >= maxPower) {
    inform();
  }
}

function inform() {
  if (DEBUG) console.log("Inform God");

  // open a new tab, and close the current one
  window.open("https://godvillegame.com/superhero");
  close();
}

var check = setInterval(checkHealth, pauseTime);

var pauseCheck = function() {
  if (DEBUG) console.log("Pausing...");
  clearInterval(check);
  check = 0;
};

var resumeCheck = function() {
  pauseCheck();
  if (DEBUG) console.log("Resuming...");
  check = setInterval(checkHealth, 15000);
};

var infoDiv = document.createElement("div");
infoDiv.innerHTML =
  '<div class="block" id="notify"><div class="block_h"><span class="l_slot"><span class="b_handle m_hover" style="display: none;" title="Drag to move this block">●</span></span><h2 class="block_title">Informer</h2><span class="r_slot"><span class="h_min m_hover" style="display: none;">↑</span></span></div><div class="block_content"><p>I will check for health every ' +
  pauseTime / 1000 +
  " seconds and if health is less then " +
  minHealth +
  "% I will notify you.</p><div>" +
  '<a style="display: inline;" class="no_link enc_link div_link" id="resume" title="Resume">Resume</a>' +
  '<a style="display: inline;" class="no_link pun_link div_link" id="pause" title="Pause">Pause</a>' +
  '<a style="display: hidden;" class="no_link enc_link div_link" id="test" title="Test">Test</a>' +
  '</div></div><div class="line"/></div></div>';
document.getElementById("right_block").appendChild(infoDiv);

document.getElementById("pause").addEventListener("click", pauseCheck, false);
document.getElementById("resume").addEventListener("click", resumeCheck, false);
document.getElementById("test").addEventListener("click", inform, false);
